import React from 'react';

import classes from './Button.css';

const button = props => {
	const cssClasses = [
		classes.Button,
		props.success ? classes.Success : null,
		props.danger ? classes.Danger : null
	];

	return (
		<button onClick={props.click} className={cssClasses.join( ' ' )}>
			{props.children}
		</button>
	);
};

export default button;