import React from 'react';
import CSSTransition from 'react-transition-group/CSSTransition';
import classes from './Modal.css';

const animationTiming = {
	enter: 300,
	exit: 300
};

const modal = props => {
	return (
		<CSSTransition
			mountOnEnter
			unmountOnExit
			in={props.show}
			timeout={animationTiming}
			classNames={{
				enter: '',
				enterActive: classes.ModalOpen,
				exit: '',
				exitActive: classes.ModalClosed
			}}
		>
			<div className={classes.Modal}>
				<button
					className={classes.CloseButtonModal}
					onClick={props.close}
				>
					X
				</button>
				<div className={classes.ModalContent}>{props.children}</div>
			</div>
		</CSSTransition>
	);
};

export default modal;