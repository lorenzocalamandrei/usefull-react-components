import React from 'react';

import classes from './Backdrop.css';

const backdrop = (props) => {
	const cssClasses = [
		classes.Backdrop,
		props.show ? classes.BackdropOpen : classes.BackdropClosed
	];

	return <div onClick={props.close} className={cssClasses.join( ' ' )}></div>;
};

export default backdrop;